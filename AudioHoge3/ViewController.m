//
//  ViewController.m
//  AudioHoge3
//
//  Created by pies on 2018/02/17.
//  Copyright © 2018年 pies. All rights reserved.
//

#import "ViewController.h"

#import "AudioManager.h"
#import "GLHogeView.h"

@interface ViewController ()
{

    AudioManager *manager;
    BOOL isFirstAppear;
}
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segDispType;

@property (nonatomic) GLHogeView *glView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    manager = [AudioManager new];
    isFirstAppear = YES;
    [self setupGLView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (isFirstAppear) {
        [manager startMeasure];
        [_glView startDisplay];
        isFirstAppear = NO;
    }
    
}

- (IBAction)changeDispType:(id)sender {
    BOOL modeSin = (_segDispType.selectedSegmentIndex == 0);
    _glView.modeSinW = modeSin;
    // todo
}

- (void)setupGLView {
    CGRect rct = _containerView.frame;
    rct.origin = CGPointZero;
    _glView = [[GLHogeView alloc] initWithFrame:rct];
    _glView.modeSinW = YES;
    _glView.bufferManager = manager.bufferManager;
    
    [_containerView addSubview:_glView];
    [_containerView bringSubviewToFront:_glView];    
}




@end
