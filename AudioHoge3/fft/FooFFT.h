//
//  FooFFT.h
//  AudioHoge2
//
//  Created by pies on 2018/02/15.
//  Copyright © 2018年 pies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FooFFT : NSObject

- (id)initWithCapacity:(unsigned int)aCapacity;
- (Float32*)process:(Float32*)input;

- (Float32*)doFFT:(Float32*)audioData frameSize:(int)frameSize;

- (void)calcFFT:(float*)data dataLength:(int)length;
@property (assign) unsigned int capacity;
@property (nonatomic) NSInteger sampleRate;
@property (readonly) Float32* realp;
@property (readonly) Float32* imagp;

@end
