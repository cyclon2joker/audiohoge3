//
//  BufferManager.m
//  AudioHoge
//
//  Created by pies on 2018/02/04.
//  Copyright © 2018年 pies. All rights reserved.
//
#include <AudioToolbox/AudioToolbox.h>
#include <libkern/OSAtomic.h>

#import "BufferManager.h"
#import "FugaFFT.h"

@interface BufferManager() {
    
    Float32 *dataBuffer;
    NSInteger bufferIndex;
    
    FugaFFT *fft;
    
    
    
    Float32*        mDrawBuffer;
    UInt32          mDrawBufferIndex;
    UInt32          mCurrentDrawBufferLen;
}

@end

@implementation BufferManager


-(id)initWithFrameSize:(NSInteger)fsz sampleR:(NSInteger)sampleR
{
    self = [super init];
    if (self) {

        _sampleRate = sampleR;
        _frameSize = fsz;
        bufferIndex = 0;
        dataBuffer = calloc(_frameSize * 2, sizeof(Float32));
        fft = [[FugaFFT alloc] initWithFrameSize:_frameSize sampleR:_sampleRate];
    }
    
    return self;
}


- (id)init{
    self = [super init];
    
    mDrawBuffer = (Float32*) calloc(4096, sizeof(Float32));
    return self;
}

-(void)copyAudioData:(Float32*)audioData
{
    int dataIdx = 0;
    if (bufferIndex != 0) {
        dataIdx = (int)_frameSize;
//        memmove(dataBuffer + _frameSize, dataBuffer, _frameSize * sizeof(Float32));
    }

    for (int i = 0; i < _frameSize; i++) {
        if (dataIdx != 0) {
            dataBuffer[i] = dataBuffer[i + dataIdx];
        }
        dataBuffer[i + dataIdx] = audioData[i];
//        NSLog(@"[%d]:%f",(i + dataIdx),dataBuffer[i + dataIdx]);
    }
    bufferIndex++;
    if (bufferIndex < 0) {
        bufferIndex = 1;
    }
    
//    if (bufferIndex < 10) {
//        bufferIndex++;
//    }
}

-(Float32*)getBufferData
{
    if (bufferIndex < 1) {
        return NULL;
    }
    return dataBuffer;
}

-(Float32*)getBufferDataFFT
{
    if (bufferIndex < 1) {
        return NULL;
    }
    return [fft calc:dataBuffer];
}




-(void)copyAudioDataToDrawBuffer:(Float32*)inData frame:(UInt32)frame
{
    if (inData == NULL) {
        return;
    }
    for (UInt32 i=0; i<frame; i++)
    {
        mDrawBuffer[i] = inData[i];
    }
}
- (Float32*)getDrawBuffer
{
    return mDrawBuffer;
}




@end
