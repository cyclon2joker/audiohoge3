//
//  BufferManager.h
//  AudioHoge
//
//  Created by pies on 2018/02/04.
//  Copyright © 2018年 pies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BufferManager : NSObject

@property (nonatomic) NSInteger frameSize;
@property (nonatomic) NSInteger sampleRate;

-(id)initWithFrameSize:(NSInteger)fsz sampleR:(NSInteger)sampleR;
-(void)copyAudioData:(Float32*)audioData;
-(Float32*)getBufferData;
-(Float32*)getBufferDataFFT;



-(void)copyAudioDataToDrawBuffer:(Float32*)inData frame:(UInt32)frame;

- (Float32*)getDrawBuffer;

@end
