//
//  GLHogeView.h
//  AudioHoge
//
//  Created by pies on 2018/01/27.
//  Copyright © 2018年 pies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BufferManager.h"
@class BufferManager;


@interface GLHogeView : UIView

@property (nonatomic) BOOL applicationResignedActive;

@property (nonatomic,weak) BufferManager *bm;

@property (nonatomic) BOOL modeSinW;

@property (nonatomic,weak) BufferManager *bufferManager;

- (void)startDisplay;
- (void)stopDisplay;


@end
