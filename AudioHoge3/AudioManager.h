//
//  AudioManager.h
//  AudioHoge2
//
//  Created by pies on 2018/02/10.
//  Copyright © 2018年 pies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

@class BufferManager;


@interface AudioManager : NSObject

- (void)createEngineAndAttachNodes;

-(AVAudioPCMBuffer*)loadAudioDataFromResource:(NSString*)resourceName;

@property (nonatomic,readonly) NSInteger sampleRate;
@property (nonatomic,readonly) NSInteger frameSize;

@property (nonatomic) BufferManager *bufferManager;

- (BOOL)startMeasure;

@end
